/**
 * The Minds Embedded Boosts SDK
 * @license MIT
 * @author Mark Harding & Ben Hayward
 */
let timer;
let currentScript;

(function MindsEmbeddedBoostsSdk() {
  currentScript = document.currentScript;

  if (!getBoostSlotElement()) {
    // We will try and pick up the embedded boost script during a future dom mutation
    return;
  }

  setupIframe();
})();

function setupIframe() {
  if (isIframeSetup()) return;

  const boostSlotElement = getBoostSlotElement();

  // mark as already setup
  boostSlotElement.setAttribute("data-ready", true);

  // build the embed app link
  let iframeSrc = boostSlotElement.getAttribute("data-app-url") ||
    currentScript.src.replace("/js/embed.js", "")
      .replace("/js/embed.min.js", "");

  if (!iframeSrc.endsWith("/")) {
    iframeSrc += "/";
  }

  const iframeHeight = boostSlotElement.getAttribute("data-height");
  const iframeWidth = boostSlotElement.getAttribute("data-width");
  const borderColor = boostSlotElement.getAttribute("data-border-color") ?? "rgba(211, 219, 277, .5)";
  const customTheme = boostSlotElement.getAttribute("data-theme") ?? null;

  // build and append query params.
  let queryParams = new URLSearchParams();
  if (customTheme) {
    queryParams.append("theme", customTheme);
  }

  // build query param string
  if (queryParams.size) {
    iframeSrc += `?${queryParams.toString()}`;
  }

  // create an iframe
  const iframe = document.createElement("iframe");
  iframe.setAttribute("name", "minds-boost-slot");
  iframe.setAttribute("src", iframeSrc);
  iframe.setAttribute(
    "style",
    `visibility: visible; width: ${iframeWidth}; height: ${iframeHeight}; border: 1px solid ${borderColor}; border-radius: 4px; color-scheme: normal;`
  );

  boostSlotElement.appendChild(iframe);
}

function getBoostSlotElement() {
  const elements = document.querySelectorAll(".minds-boost-slot");

  if (!elements.length) {
    return null;
  }

  return elements[0];
}

function isIframeSetup() {
  const boostSlotElement = getBoostSlotElement();

  if (!boostSlotElement) return false;

  return boostSlotElement.getAttribute("data-ready") === 'true';
}

const observer = new MutationObserver(function () {
  if (timer) clearTimeout(timer);

  timer = setTimeout(() => {
    if (isIframeSetup()) return;

    if (getBoostSlotElement()) {
      setupIframe();
    }
  }, 300); // Wait 300ms between mutatations
});

observer.observe(document.documentElement || document.body, {
  attributes: true,
  childList: true,
});