import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    fontSize: {
      md: ['14px', {
        lineHeight: '20px',
        letterSpacing: '0.056px'
      }],
      lg: ['16px', {
        lineHeight: '22px',
        letterSpacing: '-0.016px'
      }]
    },
    extend: {
      colors: {
        action: 'var(--color-action)',
        primary: 'var(--color-text-primary)',
        'primary-inverted': 'var(--color-text-primary-inverted)',
        'black-always': 'var(--color-black-always)'
      },
      screens: {
        // Height breakpoints.
        'h-lg': { 'raw': '(min-height: 400px)' },
        'h-md': { 'raw': '(min-height: 300px)' },
        'h-sm': { 'raw': '(min-height: 200px)' },
        // Height and width breakpoints.
        'viewport-sm': { 'raw': '(min-height: 350px) and (min-width: 220px)' }
      }  
    },
  },
  plugins: [],
  darkMode: 'class'
};
export default config;
