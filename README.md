# Embeddable Boosts

Embeddable ads for [minds.com](https://www.minds.com/) based ad networks.

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Contributing

For information on contributing, please see:

[https://developers.minds.com/docs/contributing/contributing/](https://developers.minds.com/docs/contributing/contributing/).

## License
[MIT](https://choosealicense.com/licenses/mit/)
