"use client";

import React, {PropsWithChildren, ReactNode, createContext } from "react";
import { GlobalContextType } from "../types/global-context";

/** Global context. */
export const GlobalContext = createContext<GlobalContextType>({
  siteUrl: ''
});

/** Global provider props. */
type GlobalProviderProps = PropsWithChildren<{value: GlobalContextType}>;

/**
 * Global provider wrapper component.
 * @param { GlobalProviderProps } props - Global provider props.
 * @returns { ReactNode } - Global provider component.
 */
export const GlobalProvider = ({ children, value }: GlobalProviderProps ): ReactNode => {
  return (
    <GlobalContext.Provider value={value}>
      {children}
    </GlobalContext.Provider>
  );
}

