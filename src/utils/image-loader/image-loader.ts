/**
 * Image loader function. Will just return raw src back. This exists to prevent the
 * use of the Next image cache when the site is exported.
 * @param { string } src - Image source. 
 * @param { number } width - Image width (unused).
 * @param { number } quality - Image quality (unused).
 * @returns { string } - Image source.
 */
export default function imageLoader({ src, width, quality }: { src: string, width: number, quality: number }): string {
  return src;
}