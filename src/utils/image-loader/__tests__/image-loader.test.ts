import '@testing-library/jest-dom'
import imageLoader from '../image-loader';

describe('Image Loader', () => {
  it('should return true if boost has a large thumbnail', () => {
    const src = 'https://example.minds.com/image.jpg';
    expect(imageLoader({ src: src, width: 1, quality: 1 })).toBe(src);
  });
});