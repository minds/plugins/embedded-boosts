import '@testing-library/jest-dom'
import { mockBoostEntity } from '../../../../__mocks__/models/mock-boost-entity';
import { BoostEntity } from '../../../types/boost';
import { hasBoostBody } from '../boost-body-helpers';

describe('Boost body helpers', () => {
  describe('hasBoostBody', () => {
    it('should return true if boost has a title', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        title: 'Title',
        message: '',
        link_title: ''
      };
      expect(hasBoostBody(_mockBoostEntity)).toBe(true);
    });

    it('should return true if boost has a message', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        title: '',
        message: 'Message',
        link_title: ''
      };
      expect(hasBoostBody(_mockBoostEntity)).toBe(true);
    });

    it('should return true if boost has a link title', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        title: '',
        message: '',
        link_title: 'Link title'
      };
      expect(hasBoostBody(_mockBoostEntity)).toBe(true);
    });

    it('should return false if boost has no body', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        title: '',
        message: '',
        link_title: ''
      };
      expect(hasBoostBody(_mockBoostEntity)).toBe(false);
    });
  });
})