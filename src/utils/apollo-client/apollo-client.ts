import { ApolloClient, InMemoryCache } from "@apollo/client";

/**
 * Build the Apollo client.
 * @param { string } apiBaseUrl - API base URL.
 * @returns { ApolloClient<any> } - Apollo client.
 */
export const buildApolloClient = (apiBaseUrl: string): ApolloClient<any> => {
  return new ApolloClient({
    uri: apiBaseUrl + '/api/graphql',
    cache: new InMemoryCache(),
    credentials: 'omit',
  });
}