import { BoostEntity } from "../../types/boost";

/** Base URL for media proxy. */
const PROXY_BASE_URL: string = 'https://cdn.minds.com/api/v2/media/proxy?size=800&src=';

/**
 * Whether a given boost has a header image.
 * @param { boolean } boost - Boost entity. 
 * @returns { boolean } - Whether the boost has a header image.
 */
export const hasBoostHeaderImage = (boost: BoostEntity): boolean => {
  return Boolean(getRawBoostHeaderImgSrc(boost));
}

/**
 * Get boost header image source proxied through Minds CDN when appropriate.
 * @param { BoostEntity } boost - Boost entity.
 * @returns { string|null } - Proxied boost header image source.
 */
export const getProxiedBoostHeaderImgSrc = (boost: BoostEntity): string|null => {
  const rawSrc: string|null = getRawBoostHeaderImgSrc(boost);

  // If the boost is a video, audio or batch, return the raw source.
  if (boost.custom_type === 'video' || boost.custom_type === 'audio' || boost.custom_type === 'batch') {
    return rawSrc ?? null;
  }

  // Otherwise, for a rich-embed, return the source proxied through the Minds CDN.
  return rawSrc ?
    PROXY_BASE_URL + encodeURIComponent(rawSrc) :
    null;
}

/**
 * Get raw boost header image source.
 * @param { BoostEntity } boost - Boost entity.
 * @returns { string|null } - Raw boost header image source.
 */
const getRawBoostHeaderImgSrc = (boost: BoostEntity): string|null => {
  if (boost.custom_type === 'audio') {
    return boost.custom_data?.thumbnail_src ?? null;
  }
  return boost.thumbnails?.['large'] ?? boost.thumbnails?.['xlarge'] ?? boost.thumbnail_src ?? null;
}
