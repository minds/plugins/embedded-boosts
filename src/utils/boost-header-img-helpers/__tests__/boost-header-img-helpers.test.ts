import '@testing-library/jest-dom'
import { getProxiedBoostHeaderImgSrc, hasBoostHeaderImage } from '../boost-header-img-helpers';
import { mockBoostEntity } from '../../../../__mocks__/models/mock-boost-entity';
import { BoostEntity } from '../../../types/boost';

describe('Boost header image helpers', () => {
  describe('hasBoostHeaderImage', () => {
    it('should return true if boost has a large thumbnail', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        thumbnail_src: '',
        thumbnails: { large: 'https://www.minds.com/image.jpg' }
      };
      expect(hasBoostHeaderImage(_mockBoostEntity)).toBe(true);
    });

    it('should return true if boost has a xlarge thumbnail', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        thumbnail_src: '',
        thumbnails: { xlarge: 'https://www.minds.com/image.jpg' }
      };
      expect(hasBoostHeaderImage(_mockBoostEntity)).toBe(true);
    });

    it('should return true if boost has a thumbnail_src', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        thumbnail_src: 'https://www.minds.com/image.jpg',
        thumbnails: null
      };
      expect(hasBoostHeaderImage(_mockBoostEntity)).toBe(true);
    });

    it('should return false if boost has no thumbnail image', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        thumbnail_src: '',
        thumbnails: null
      };
      expect(hasBoostHeaderImage(_mockBoostEntity)).toBe(false);
    });
  });

  describe('getProxiedBoostHeaderImgSrc', () => {
    it('should return proxied img src if boost has a large thumbnail', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        thumbnail_src: '',
        thumbnails: { large: 'https://www.minds.com/image.jpg' }
      };
      expect(getProxiedBoostHeaderImgSrc(_mockBoostEntity)).toBe(
        'https://cdn.minds.com/api/v2/media/proxy?size=800&src=https%3A%2F%2Fwww.minds.com%2Fimage.jpg'
      );
    });

    it('should return true if boost has a xlarge thumbnail', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        thumbnail_src: '',
        thumbnails: { xlarge: 'https://www.minds.com/image.jpg' }
      };
      expect(getProxiedBoostHeaderImgSrc(_mockBoostEntity)).toBe(
        'https://cdn.minds.com/api/v2/media/proxy?size=800&src=https%3A%2F%2Fwww.minds.com%2Fimage.jpg'
      );
    });

    it('should return true if boost has a thumbnail_src', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        thumbnail_src: 'https://www.minds.com/image.jpg',
        thumbnails: null
      };
      expect(getProxiedBoostHeaderImgSrc(_mockBoostEntity)).toBe(
        'https://cdn.minds.com/api/v2/media/proxy?size=800&src=https%3A%2F%2Fwww.minds.com%2Fimage.jpg'
      );
    });

    it('should return false if boost has no thumbnail image', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        thumbnail_src: '',
        thumbnails: null
      };
      expect(getProxiedBoostHeaderImgSrc(_mockBoostEntity)).toBe(null);
    });

    it('should return raw src if boost is an audio', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        custom_type: 'audio',
        custom_data: { thumbnail_src: 'https://www.minds.com/image.jpg' }
      };
      expect(getProxiedBoostHeaderImgSrc(_mockBoostEntity)).toBe(
        'https://www.minds.com/image.jpg'
      );
    });

    it('should return raw src if boost is a video', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        custom_type: 'video',
        thumbnails: { large: 'https://www.minds.com/image.jpg' }
      };
      expect(getProxiedBoostHeaderImgSrc(_mockBoostEntity)).toBe(
        'https://www.minds.com/image.jpg'
      );
    });

    it('should return raw src if boost is of batch type', () => {
      const _mockBoostEntity: BoostEntity = {
        ...mockBoostEntity,
        custom_type: 'batch',
        thumbnails: { large: 'https://www.minds.com/image.jpg' }
      };
      expect(getProxiedBoostHeaderImgSrc(_mockBoostEntity)).toBe(
        'https://www.minds.com/image.jpg'
      );
    });
  });
});