import { BoostGoalButtonText } from "../../enums/goalButtonText";
import { BoostEntity } from "../../types/boost";

/**
 * Get empty state mock boost.
 * @param { string } siteUrl - Site URL.
 * @returns { BoostEntity } - Empty state mock boost.
 */
export const getEmptyStateMockBoost = (siteUrl: string): BoostEntity => ({
	guid: '1234567890123456',
	message: 'Create a post and promote it here.',
	title: 'Boost your message',
	custom_type: 'batch',
	thumbnail_src: '/plugins/embedded-boosts/advertise-boost.png',
	perma_url: siteUrl + '#boost',
	goal_button_text: BoostGoalButtonText.GET_STARTED,
	goal_button_url: siteUrl + '#boost'
});