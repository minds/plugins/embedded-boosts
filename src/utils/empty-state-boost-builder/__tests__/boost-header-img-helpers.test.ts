import '@testing-library/jest-dom'
import { getEmptyStateMockBoost } from '../empty-state-boost-builder';

describe('Empty state boost builder', () => {
  it('should get empty state mock boost', () => {
    expect(getEmptyStateMockBoost('https://example.minds.com/')).toEqual({
      "custom_type": "batch",
      "goal_button_text": 5,
      "goal_button_url": "https://example.minds.com/#boost",
      "guid": "1234567890123456",
      "message": "Create a post and promote it here.",
      "perma_url": "https://example.minds.com/#boost",
      "thumbnail_src": "/plugins/embedded-boosts/advertise-boost.png",
      "title": "Boost your message",
    });
  });
});