/** Global context type. */
export type GlobalContextType = {
  siteUrl: string;
}