import { BoostGoalButtonText } from "../enums/goalButtonText";

/** Boost entity. */
export type BoostEntity = {
	guid: string;
	message: string;
	title: string;
	link_title?: string|null;
	custom_type: 'video' | 'audio' | 'batch' | null | false;
	custom_data?: any;
	thumbnail_src: string;
	perma_url?: string | undefined | null | false;
	thumbnails?: { [key: string]: string } | null;
	boosted_guid?: string;
	goal_button_text?: BoostGoalButtonText | null;
	goal_button_url?: string | null;
	urn?: string;
}