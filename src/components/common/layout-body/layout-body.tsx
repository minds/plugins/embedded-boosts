"use client";

import { ThemeProvider } from "next-themes";
import { NextFont } from "next/dist/compiled/@next/font";
import { Inter, JetBrains_Mono } from "next/font/google";
import { ReadonlyURLSearchParams, useSearchParams } from "next/navigation";
import { PropsWithChildren, ReactNode, Suspense } from "react";

/** Inter font. */
const inter = Inter({ subsets: ["latin"] });

/** JetBrains Mono font. */
const jetbrainsMono = JetBrains_Mono({ subsets: ["latin"] });

/**
 * Client side wrapper for body. Will set the theme appropriately.
 * @param { PropsWithChildren } props - props. 
 * @returns { ReactNode } - Layout body component.
 */
export default function LayoutBody({ children }: PropsWithChildren): ReactNode {
  const BodyContentWrapper = ({ children }: PropsWithChildren): ReactNode => {
    const searchParams: ReadonlyURLSearchParams = useSearchParams();
    let font: NextFont = inter;
    let themeName: string = 'light';
 
    switch(searchParams.get('theme')) {
      case 'fishtank':
        font = jetbrainsMono;
        themeName = 'fishtank';
        break;
    }

    if (typeof window !== "undefined") {
      document?.body?.classList.add(font.className);
    }

    return ( 
      <ThemeProvider
        attribute="class"
        enableSystem={false}
        disableTransitionOnChange={true}
        themes={['light', 'dark', 'fishtank']}
        defaultTheme={themeName}
      >
        {children}
      </ThemeProvider>
    );
  }

  return (
    <body className="h-screen w-screen">
      <Suspense fallback={<></>}>
        <BodyContentWrapper>{children}</BodyContentWrapper>
      </Suspense>
    </body>
  );
}