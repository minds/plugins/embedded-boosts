import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import { ExternalNavigationClickArea } from '../external-navigation-click-area';
import { mockBoostEntity } from '../../../../../__mocks__/models/mock-boost-entity';
import { BoostEntity } from '../../../../types/boost';
import { GlobalContext } from '../../../../providers/global-provider';

describe('ExternalNavigationClickArea', () => {
  beforeEach(() => {
    Object.defineProperty(window, "open", {
      value: jest.fn(),
    });
    jest.spyOn(console, 'error').mockImplementation(() => {}); 
    global.fetch = jest.fn(() => Promise.resolve({
        json: () => Promise.resolve([])
    }));
  
  });

  it('renders a clickable wrapper', async() => {
    const { container } = render(
      <ExternalNavigationClickArea boost={mockBoostEntity}>
        <p data-testid="test-paragraph">Test</p>
      </ExternalNavigationClickArea>
    );

    expect(container).toMatchSnapshot();
    expect(screen.getByTestId('test-paragraph')).toBeInTheDocument();

    const clickWrapperElement: HTMLElement = screen.getByTestId('external-navigation-click-wrapper');
    expect(clickWrapperElement).toBeInTheDocument();
  });

  it('renders a clickable wrapper that goes to goal_button_url_when_appropriate', async() => {
    const { container } = render(
      <ExternalNavigationClickArea boost={mockBoostEntity}>
      </ExternalNavigationClickArea>
    );
    expect(container).toMatchSnapshot();

    const clickWrapperElement: HTMLElement = screen.getByTestId('external-navigation-click-wrapper');
    expect(clickWrapperElement).toBeInTheDocument();
    clickWrapperElement.click();

    expect(window.open).toHaveBeenNthCalledWith(1, mockBoostEntity.goal_button_url, '_blank');
  });

  it('renders a clickable wrapper that goes to perma_url when appropriate', async() => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      goal_button_url: ''
    };
    const { container } = render(
      <ExternalNavigationClickArea boost={_mockBoostEntity}
      ></ExternalNavigationClickArea>
    );
    expect(container).toMatchSnapshot();

    const clickWrapperElement: HTMLElement = screen.getByTestId('external-navigation-click-wrapper');
    expect(clickWrapperElement).toBeInTheDocument();
    clickWrapperElement.click();

    expect(window.open).toHaveBeenNthCalledWith(1, mockBoostEntity.perma_url, '_blank');
  });

  it('renders a clickable wrapper that goes to newsfeed entry for item when appropriate', async() => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      goal_button_url: '',
      perma_url: ''
    };
    const { container } = render(
      <GlobalContext.Provider value={{siteUrl: 'https://www.minds.com'}}>
        <ExternalNavigationClickArea boost={_mockBoostEntity}
        ></ExternalNavigationClickArea>
      </GlobalContext.Provider>
    );
    expect(container).toMatchSnapshot();

    const clickWrapperElement: HTMLElement = screen.getByTestId('external-navigation-click-wrapper');
    expect(clickWrapperElement).toBeInTheDocument();
    clickWrapperElement.click();

    expect(window.open).toHaveBeenNthCalledWith(1, 'https://www.minds.com/newsfeed/1234567890123456', '_blank');
  });
});