"use client";

import { PropsWithChildren, ReactNode, useCallback, useContext, useEffect } from "react";
import { BoostEntity } from "../../../types/boost";
import { GlobalContext } from "../../../providers/global-provider";
import { GlobalContextType } from "../../../types/global-context";
import posthog from "posthog-js";

/* Props for ExternalNavigationClickArea. */
type ExternalNavigationClickAreaProps = PropsWithChildren<{
  boost: BoostEntity;
  target?: string;
}>;

export const getXsrfCookie = () => {
  return document.cookie.replace(
    /(?:(?:^|.*;\s*)XSRF-TOKEN\s*=\s*([^;]*).*$)|^.*$/,
    "$1",
  );
};


/**
 * External navigation click area component. Will navigate clicks on children to the boosts URL.* @param { ExternalNavigationClickAreaProps } props - External navigation click area props.
 * @returns { ReactNode } - External navigation click area component.
 */
export const ExternalNavigationClickArea = ({ boost, target, children }: ExternalNavigationClickAreaProps): ReactNode => {
  const globalContext: GlobalContextType = useContext(GlobalContext);

  /**
   * Handle click event by opening the URL in a new tab.
   * @returns { void }
   */
  const handleClick = useCallback(() => {
    // Send to posthog
    posthog.capture('embedded_boost_click', {
      cm_campaign: boost.urn
    });

    // Send to minds
    fetch(globalContext.siteUrl + '/api/v3/analytics/click/' + boost.guid + '/?external=true', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-XSRF-TOKEN': getXsrfCookie(),
      },
      body: JSON.stringify({
        client_meta: {
          campaign: boost.urn,
        }
      })
    });

    window.open(
      boost.goal_button_url ||
      boost.perma_url ||
      `${globalContext.siteUrl}/newsfeed/${boost.guid}`,
      target ?? '_blank'
    );
  }, [boost, globalContext.siteUrl, target]);

  useEffect(() => {
    const messageEventHandler = (event: any) => {
      if (event?.data?.type === "forceClick") {
        handleClick(); 
      }
    };

    window.addEventListener("message", messageEventHandler);
    return () => {
      window.removeEventListener("message", messageEventHandler)
    }
  }, [handleClick]);

  return <div onClick={handleClick} className="cursor-pointer h-full w-full" data-testid="external-navigation-click-wrapper">{children}</div>;
}