import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import { GlobalContext } from '../../../../providers/global-provider';
import { MindsLinkify } from '../linkify';

describe('MindsLinkify', () => {
  it('renders no links', async() => {
    const text: string = 'hello world';
    const { container } = render(
      <MindsLinkify>{text}</MindsLinkify>
    );

    expect(container).toMatchSnapshot();
    const anchorElement: HTMLElement|null = screen.queryByRole('link');
    expect(anchorElement).not.toBeInTheDocument();
  });

  it('renders a href', async() => {
    const url: string = 'https://www.minds.com'
    const { container } = render(
      <MindsLinkify>{url}</MindsLinkify>
    );

    expect(container).toMatchSnapshot();
    const anchorElement: HTMLElement = screen.getByRole('link');
    expect(anchorElement).toBeInTheDocument();
    expect(anchorElement.innerHTML).toBe(url);
    expect(anchorElement).toHaveAttribute('href', url);
  });

  it('renders an at tag', async() => {
    const tag: string = '@minds';
    const { container } = render(
      <GlobalContext.Provider value={{siteUrl: 'https://www.minds.com'}}>
        <MindsLinkify>{tag}</MindsLinkify>
      </GlobalContext.Provider>
    );

    expect(container).toMatchSnapshot();
    const anchorElement: HTMLElement = screen.getByRole('link');
    expect(anchorElement).toBeInTheDocument();
    expect(anchorElement.innerHTML).toBe(tag);
    expect(anchorElement).toHaveAttribute('href', 'https://www.minds.com/minds');
  });

  it('renders a hashtag', async() => {
    const tag: string = '#minds';
    const { container } = render(
      <GlobalContext.Provider value={{siteUrl: 'https://www.minds.com'}}>
        <MindsLinkify>{tag}</MindsLinkify>
      </GlobalContext.Provider>
    );

    expect(container).toMatchSnapshot();
    const anchorElement: HTMLElement = screen.getByRole('link');
    expect(anchorElement).toBeInTheDocument();
    expect(anchorElement.innerHTML).toBe(tag);
    expect(anchorElement).toHaveAttribute('href', 'https://www.minds.com/discovery/search?f=top&t=all&q=%23minds');
  });

  it('renders a mix of links', async() => {
    const text: string = 'hello world https://www.minds.com @minds #minds';
    const { container } = render(
      <GlobalContext.Provider value={{siteUrl: 'https://www.minds.com'}}>
        <MindsLinkify>{text}</MindsLinkify>
      </GlobalContext.Provider>
    );

    expect(container).toMatchSnapshot();
    const anchorElements: HTMLElement[]|null = screen.getAllByRole('link');
    expect(anchorElements).toHaveLength(3);
    expect(anchorElements[0]).toBeInTheDocument();
    expect(anchorElements[0].innerHTML).toBe('https://www.minds.com');
    expect(anchorElements[0]).toHaveAttribute('href', 'https://www.minds.com');

    expect(anchorElements[1]).toBeInTheDocument();
    expect(anchorElements[1].innerHTML).toBe('@minds');
    expect(anchorElements[1]).toHaveAttribute('href', 'https://www.minds.com/minds');

    expect(anchorElements[2]).toBeInTheDocument();
    expect(anchorElements[2].innerHTML).toBe('#minds');
    expect(anchorElements[2]).toHaveAttribute('href', 'https://www.minds.com/discovery/search?f=top&t=all&q=%23minds');
  });
});