"use client";

import Linkify from "linkify-react";
import "linkify-plugin-hashtag";
import "linkify-plugin-mention";
import { PropsWithChildren, useContext } from "react";
import { GlobalContext } from "../../../providers/global-provider";
import { GlobalContextType } from "../../../types/global-context";

/**
 * Linkify component. Will convert URLs, hashtags and mentions to links.
 * @param { PropsWithChildren } props - simple children prop.
 * @returns { JSX.Element } - MindsLinkify component.
 */
export const MindsLinkify = ({ children }: PropsWithChildren) => {
  const globalContext: GlobalContextType = useContext(GlobalContext);

  const options: Object = {
      formatHref: {
          hashtag: (href: string) => globalContext.siteUrl + "/discovery/search?f=top&t=all&q=%23" + href.substring(1),
          mention: (href: string) => globalContext.siteUrl + href,
      },
      attributes: {
        /**
         * Prevent outward propagation to the parent
         * click handler that navigates to the boost URL.
         */ 
        onClick: (event: any) => event.stopPropagation(),
      },
      target: "_blank",
      className: "text-action hover:underline",
      nl2br: true // preserve \n line breaks.
  };
  
  return <Linkify options={options}>{ children }</Linkify>;
}