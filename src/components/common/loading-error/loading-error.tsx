import { ReactNode } from "react";

/**
 * Loading error component.
 * @returns { ReactNode } - Loading error component.
 */
export const LoadingError = (): ReactNode => {
  return <p className="text-md text-primary font-bold" data-testid="boost-loader-error">There was an error rendering this ad unit</p>;
}