import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import { LoadingError } from '../loading-error';

describe('LoadingError', () => {
  it('renders no links', async() => {
    const { container } = render(<LoadingError/>);

    expect(container).toMatchSnapshot();
    const loadingErrorTextElement: HTMLElement|null = await screen.findByTestId('boost-loader-error');
    expect(loadingErrorTextElement).toBeInTheDocument();
    expect(loadingErrorTextElement.innerHTML).toBe('There was an error rendering this ad unit');
  });
});