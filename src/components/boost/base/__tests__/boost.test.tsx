import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import Boost from '../boost';
import { BoostEntity } from '../../../../types/boost';
import { mockBoostEntity } from '../../../../../__mocks__/models/mock-boost-entity';

describe('Boost', () => {
  beforeEach(() => {
    jest.spyOn(console, 'error').mockImplementation(() => {});
  });

  it('renders a boost', async() => {
    const _mockBoostEntity: BoostEntity = mockBoostEntity;
    const { container } = render(<Boost boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    expect(screen.getByTestId('boost-header-container')).toBeInTheDocument();
    expect(screen.getByTestId('boost-body-title')).toBeInTheDocument();
    expect(screen.getByTestId('boost-footer-cta')).toBeInTheDocument();
  });

  it('renders a boost without an image', async() => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      thumbnail_src: '',
      thumbnails: null
    };
    const { container } = render(<Boost boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    expect(screen.queryByTestId('boost-header-container')).not.toBeInTheDocument();
    expect(screen.getByTestId('boost-body-title')).toBeInTheDocument();
    expect(screen.getByTestId('boost-footer-cta')).toBeInTheDocument();
  });

  it('renders without CTA when boost has no goal button text', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      goal_button_text: null
    };
    const { container } = render(<Boost boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    expect(screen.getByTestId('boost-header-container')).toBeInTheDocument();
    expect(screen.getByTestId('boost-body-title')).toBeInTheDocument();
    expect(screen.queryByTestId('boost-footer-cta')).not.toBeInTheDocument();
  });
});