import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import { mockBoostEntity } from '../../../../../__mocks__/models/mock-boost-entity'
import { BoostEntity } from '../../../../types/boost';
import { BoostHeader } from '../header';

describe('BoostHeader', () => {
  beforeEach(() => {
    jest.spyOn(console, 'error').mockImplementation(() => {});
  });

  it('renders an image', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      title: "",
      message: "message"
    };

    const { container } = render(<BoostHeader boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const imageElement: HTMLElement = screen.getByTestId('boost-header-image');
    expect(imageElement).toBeInTheDocument();
    expect(imageElement.nodeName).toBe('IMG');
    expect(imageElement).toHaveAttribute('src', 'https://cdn.minds.com/api/v2/media/proxy?size=800&src=https%3A%2F%2Fwww.minds.com%2Fstatic%2Fen%2Fassets%2Flogos%2Fbulb.svg');
  });

  it('renders a video', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      title: "",
      message: "message",
      custom_type: "video"
    };

    const { container } = render(<BoostHeader boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const imageElement: HTMLElement = screen.getByTestId('boost-header-image');
    expect(imageElement).toBeInTheDocument();
    expect(imageElement.nodeName).toBe('IMG');
    expect(imageElement).toHaveAttribute('src', 'https://www.minds.com/static/en/assets/logos/bulb.svg');

    const playButtonElement: HTMLElement = screen.getByTestId('boost-header-video-play-button');
    expect(playButtonElement).toBeInTheDocument();
    expect(playButtonElement.nodeName).toBe('IMG');
    expect(playButtonElement).toHaveAttribute('src', '/_next/image?url=%2Fplugins%2Fembedded-boosts%2Fplay_circle_outline.png&w=256&q=75');
  });

  it('renders with a height of 100% when there is no title or message', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      title: "",
      message: "",
      custom_type: null
    };

    const { container } = render(<BoostHeader boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const containerElement: HTMLElement = screen.getByTestId('boost-header-container');
    expect(containerElement).toBeInTheDocument();
    expect(containerElement).toHaveClass('h-[100%]');
  });

  it('renders with a height of 30% when there is only a title ', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      title: "title",
      message: "",
      custom_type: null
    };

    const { container } = render(<BoostHeader boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const containerElement: HTMLElement = screen.getByTestId('boost-header-container');
    expect(containerElement).toBeInTheDocument();
    expect(containerElement).not.toHaveClass('h-[100%]');
    expect(containerElement).toHaveClass('flex items-center w-full h-[50%] h-lg:h-[30%]');
  });

  it('renders with a height of 30% when there is only a message ', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      title: "",
      message: "message",
      custom_type: null
    };

    const { container } = render(<BoostHeader boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const containerElement: HTMLElement = screen.getByTestId('boost-header-container');
    expect(containerElement).toBeInTheDocument();
    expect(containerElement).not.toHaveClass('h-[100%]');
    expect(containerElement).toHaveClass('flex items-center w-full h-[50%] h-lg:h-[30%]');
  });

  it('renders with a height of 30% when there is a title and a message ', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      title: "title",
      message: "message",
      custom_type: null
    };

    const { container } = render(<BoostHeader boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const containerElement: HTMLElement = screen.getByTestId('boost-header-container');
    expect(containerElement).toBeInTheDocument();
    expect(containerElement).not.toHaveClass('h-[100%]');
    expect(containerElement).toHaveClass('flex items-center w-full h-[50%] h-lg:h-[30%]');
  });
});