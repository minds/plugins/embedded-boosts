import Image from "next/image";
import { BoostEntity } from "../../../types/boost";
import clsx from "clsx";
import { getProxiedBoostHeaderImgSrc } from "../../../utils/boost-header-img-helpers/boost-header-img-helpers";
import { hasBoostBody } from "../../../utils/boost-body-helpers/boost-body-helpers";

/** Props for boost header. */
export type BoostHeaderProps = { boost: BoostEntity }

/**
 * Boost header component. Contains media related to Boost.
 * @param { BoostHeaderProps } props - Boost header props.
 * @returns { JSX.Element } - Boost header component.
 */
export const BoostHeader = ({ boost }: BoostHeaderProps): JSX.Element => {
  const imgSrc: string|null = getProxiedBoostHeaderImgSrc(boost);
  
  if (!imgSrc) {
    return <></>;
  }

  return (
    <div className={clsx("flex items-center w-full ", {
        'h-[100%]': !hasBoostBody(boost),
        'h-[50%] h-lg:h-[30%]': hasBoostBody(boost)
      })}
      data-testid="boost-header-container"
    >
      <div
        className="relative w-full h-full max-h-[300px] sm:max-h-[500px] bg-black-always"
      >
        { Boolean(boost.custom_type === 'video') &&
          <Image
            className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-10"
            src="/plugins/embedded-boosts/play_circle_outline.png"
            alt="Play button"
            width={65}
            height={64}
            data-testid="boost-header-video-play-button"
          ></Image>
        }
        <Image
          className="object-cover h-md:object-contain"
          src={imgSrc}
          alt="Advertised image"
          fill
          priority={true}
          data-testid="boost-header-image"
        ></Image>
      </div>
    </div>
  );
}