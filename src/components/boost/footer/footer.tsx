import { ReactNode } from "react";
import { BoostGoalButtonText } from "../../../enums/goalButtonText";
import { BoostEntity } from "../../../types/boost";

/** Boost footer props. */
export type BoostFooterProps = { boost: BoostEntity };

/**
 * Boost footer component, contains core CTA.
 * @param { BoostFooterProps } props - Boost footer props.  
 * @returns { ReactNode } - Boost footer component.
 */
export const BoostFooter = ({ boost }: BoostFooterProps): ReactNode => {
  if (!Boolean(boost.goal_button_text)) return null;

  /**
   * Parse CTA text from goal button text enum.
   * @param { BoostGoalButtonText|null|undefined } goalButtonText - Goal button text enum.
   * @returns { string|null } - Parsed CTA text.
   */
  const parseCtaText = (goalButtonText: BoostGoalButtonText | null | undefined): string|null => {
    switch (goalButtonText) {
      case BoostGoalButtonText.SUBSCRIBE_TO_MY_CHANNEL:
        return 'Subscribe';
      case BoostGoalButtonText.GET_CONNECTED:
        return 'Get connected';
      case BoostGoalButtonText.STAY_IN_THE_LOOP:
        return 'Stay in the loop';
      case BoostGoalButtonText.LEARN_MORE:
        return 'Learn more';
      case BoostGoalButtonText.GET_STARTED:
        return 'Get started';
      case BoostGoalButtonText.SIGN_UP:
        return 'Sign up';
      case BoostGoalButtonText.TRY_FOR_FREE:
        return 'Try for free';
      case BoostGoalButtonText.SHOP_NOW:
        return 'Shop now';
      case BoostGoalButtonText.BUY_NOW:
        return 'Buy now';
      default:
        return null;
    }
  }

  return (
    <div className="mx-[16px] h-lg:my-[16px] my-[8px] min-h-[36px]">
      <button
        className="bg-action text-primary-inverted w-full font-bold py-[6px] px-[38px] rounded-[99px] hover:brightness-90 active:brightness-[0.8]"
        data-testid="boost-footer-cta"
      >{parseCtaText(boost.goal_button_text)}</button
      >
    </div>
  );
}