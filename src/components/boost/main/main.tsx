"use client";

import { useQuery } from "@apollo/client";
import { LoadingError } from "../../common/loading-error/loading-error";
import { INIT_QUERY } from "../../../graphql/init-query";
import { useTheme } from "next-themes";
import { ExternalNavigationClickArea } from "../../common/external-navigation-click-area/external-navigation-click-area";
import Boost from "../base/boost";
import { ReactNode, useEffect, useState } from "react";
import { GlobalProvider } from "../../../providers/global-provider";
import { BoostEntity } from "../../../types/boost";
import { getEmptyStateMockBoost } from "../../../utils/empty-state-boost-builder/empty-state-boost-builder";
import { isCustomTheme } from "../../../enums/customTheme";
import posthog from "posthog-js";

/**
 * Main wrapper for Boost.
 * @param { string } siteUrl - Site URL.
 * @returns { ReactNode } - Main component.
 */
export const Main = ({ siteUrl }: { siteUrl: string }): ReactNode => {
  const { theme, setTheme } = useTheme();
  const { data, loading, error } = useQuery(INIT_QUERY);
  const [boost, setBoost] = useState<BoostEntity|null>(null);

  if (!theme || !isCustomTheme(theme)) {
    setTheme(data?.color_scheme === 'DARK' ? 'dark' : 'light');
  }

  useEffect(() => {
    // Setup posthog
    if (typeof window !== "undefined" && data?.posthog_host && boost) {
      posthog.init(data.posthog_api_key, {
        api_host: "https://" + data.posthog_host,
        capture_pageview: false,
        autocapture: false,
        advanced_disable_feature_flags: true,
      });
      // Send event to posthog
      posthog.capture('embedded_boost_view', {
        cm_campaign: boost.urn,
        tenant_id: data.tenant_id,
        $set: {
          tenant_id: data.tenant_id,
        }
      });
      // Send event to Minds
      fetch(siteUrl + '/api/v2/analytics/views/boost/' + boost.boosted_guid + '/?external=true', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          client_meta: {
            campaign: boost.urn,
          }
        })
      });
    }
  }, [boost]);

  useEffect(() => {
    // will override custom theme set colors - this is to give the 
    // user direct control over the action color. If we want to change
    // this in the future, only do this if the theme is not custom.
    if (data?.primary_color) {
      document.documentElement.style.setProperty('--color-action', data.primary_color);
    }

    if (!loading) {
      setBoost(
        data?.boosts?.edges?.[0]?.node?.legacy ?
          JSON.parse(data?.boosts?.edges?.[0]?.node?.legacy) :
          getEmptyStateMockBoost(siteUrl)
      );
    }
  }, [data, loading]);

  if (loading) {
    return <></>;
  }

  if (error) {
    return <LoadingError/>
  }

  return (
    <GlobalProvider value={{ siteUrl: siteUrl }}>
      { boost ? 
        <ExternalNavigationClickArea boost={boost}>
          <main className='flex h-full w-full'>
            <Boost boost={boost}/>
          </main>
        </ExternalNavigationClickArea>
        : <LoadingError/>
      }
    </GlobalProvider>
  );
}