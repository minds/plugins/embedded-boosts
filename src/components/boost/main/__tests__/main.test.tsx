import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import { mockBoostEntity } from '../../../../../__mocks__/models/mock-boost-entity'
import { ApolloQueryResult } from '@apollo/client';
import { Main } from '../main';
import { MockedProvider } from "@apollo/client/testing";
import { INIT_QUERY } from '../../../../graphql/init-query';
import { ThemeProvider } from 'next-themes';

describe('Main', () => {
  const SUCCESSFUL_MOCK_RESPONSE: ApolloQueryResult<any> = {
    data: {
      primary_color: '#ff0000',
      color_scheme: 'DARK',
      boosts: {
        edges: [
          {
            node: {
              legacy: JSON.stringify(mockBoostEntity)
            }
          }
        ]
      }
    },
    loading: false,
    error: undefined,
    networkStatus: 7
  };

  let localStorageMock: { [key: string]: string } = {}

  beforeAll(() => {
    window.matchMedia = query => ({
      matches: false,
      media: query,
      onchange: null,
      addListener: jest.fn(), // deprecated
      removeListener: jest.fn(), // deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
    });

    // Create mocks of localStorage getItem and setItem functions
    global.Storage.prototype.getItem = jest.fn(
      (key: string) => localStorageMock[key]
    )
    global.Storage.prototype.setItem = jest.fn((key: string, value: string) => {
      localStorageMock[key] = value
    })
  });

  beforeEach(() => {
    localStorageMock = {};
  })

  it('renders with data', async() => {
    const siteUrl: string = 'https://example.minds.com';

    const mocks = [
      {
        request: {
          query: INIT_QUERY,
        },
        result: {
          data: SUCCESSFUL_MOCK_RESPONSE.data
        }
      }
    ];

    const { container } = render(
      <ThemeProvider>
        <MockedProvider mocks={mocks} addTypename={false}>
          <Main siteUrl={siteUrl}/>
        </MockedProvider>
      </ThemeProvider>
    );

    expect(await screen.findByText(mockBoostEntity.message)).toBeInTheDocument();
    expect(screen.queryByText('There was an error rendering this ad unit')).not.toBeInTheDocument();
    expect(screen.queryByText('...')).not.toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('renders with error state', async() => {
    const siteUrl: string = 'https://example.minds.com';

    const mocks = [
      {
        request: {
          query: INIT_QUERY,
        },
        result: {
          data: SUCCESSFUL_MOCK_RESPONSE.data
        },
        error: new Error('Error')
      }
    ];

    const { container } = render(
      <ThemeProvider>
        <MockedProvider mocks={mocks} addTypename={false}>
          <Main siteUrl={siteUrl}/>
        </MockedProvider>
      </ThemeProvider>
    );

    expect(await screen.findByText('There was an error rendering this ad unit')).toBeInTheDocument();
    expect(screen.queryByText('...')).not.toBeInTheDocument();
    expect(screen.queryByText(mockBoostEntity.message)).not.toBeInTheDocument();
  });

  it('renders with successful response containing no boosts', async() => {
    const siteUrl: string = 'https://example.minds.com';

    const mocks = [
      {
        request: {
          query: INIT_QUERY,
        },
        result: {
          data: {
            ...SUCCESSFUL_MOCK_RESPONSE.data,
            boosts: { edges: [] }
          }    
        }
      }
    ];

    const { container } = render(
      <ThemeProvider>
        <MockedProvider mocks={mocks} addTypename={false}>
          <Main siteUrl={siteUrl}/>
        </MockedProvider>
      </ThemeProvider>
    );

    expect(await screen.findByText('Create a post and promote it here.')).toBeInTheDocument();
    expect(screen.queryByText('There was an error rendering this ad unit')).not.toBeInTheDocument();
    expect(screen.queryByText('...')).not.toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('sets theme when rendering when not using a custom theme', async() => {
    localStorageMock['theme'] = 'light';
    const siteUrl: string = 'https://example.minds.com';
    const mocks = [
      {
        request: {
          query: INIT_QUERY,
        },
        result: {
          data: {
            ...SUCCESSFUL_MOCK_RESPONSE.data,
            boosts: { edges: [] }
          }    
        }
      }
    ];

    const { container } = render(
      <ThemeProvider>
        <MockedProvider mocks={mocks} addTypename={false}>
          <Main siteUrl={siteUrl}/>
        </MockedProvider>
      </ThemeProvider>
    );

    expect(await screen.findByText('Create a post and promote it here.')).toBeInTheDocument();
    expect(localStorageMock['theme']).toBe('dark');
    expect(container).toMatchSnapshot();
  });

  it('does not set theme when rendering when using a custom theme', async() => {
    localStorageMock['theme'] = 'fishtank';
    const siteUrl: string = 'https://example.minds.com';
    const mocks = [
      {
        request: {
          query: INIT_QUERY,
        },
        result: {
          data: {
            ...SUCCESSFUL_MOCK_RESPONSE.data,
            boosts: { edges: [] }
          }    
        }
      }
    ];

    const { container } = render(
      <ThemeProvider>
        <MockedProvider mocks={mocks} addTypename={false}>
          <Main siteUrl={siteUrl}/>
        </MockedProvider>
      </ThemeProvider>
    );

    expect(await screen.findByText('Create a post and promote it here.')).toBeInTheDocument();
    expect(localStorageMock['theme']).toBe('fishtank');
    expect(container).toMatchSnapshot();
  });

  it('sets accent color', async() => {
    const siteUrl: string = 'https://example.minds.com';
    const primaryColor: string = '#123456';
    const mocks = [
      {
        request: {
          query: INIT_QUERY,
        },
        result: {
          data: {
            ...SUCCESSFUL_MOCK_RESPONSE.data,
            boosts: { edges: [] },
            primary_color: primaryColor
          }    
        }
      }
    ];

    const { container } = render(
      <ThemeProvider>
        <MockedProvider mocks={mocks} addTypename={false}>
          <Main siteUrl={siteUrl}/>
        </MockedProvider>
      </ThemeProvider>
    );

    expect(await screen.findByText('Create a post and promote it here.')).toBeInTheDocument();
    expect(document.documentElement.style.getPropertyValue('--color-action')).toBe(primaryColor);
  });
});