import clsx from "clsx";
import { BoostEntity } from "../../../types/boost";
import { MindsLinkify } from "../../common/linkify/linkify";
import { hasBoostBody } from "../../../utils/boost-body-helpers/boost-body-helpers";

/** Props for boost body. */
export type BoostBodyProps = { boost: BoostEntity };

/**
 * Boost body component - contains main section with title and text.
 * @param { BoostBodyProps } props - Boost body props.
 * @returns { JSX.Element } - Boost body component.
 */
export const BoostBody = ({ boost }: BoostBodyProps): JSX.Element => {
    return (
    <div className={clsx("", {
        'flex-1 overflow-auto mt-[12px] px-[16px] mb-0 h-md:mb-[8px]': hasBoostBody(boost)
      })}
      data-testid="boost-body-container"
    >
      { Boolean(boost.title || boost.link_title) && 
        <h1 className="text-lg text-primary font-bold mb-[12px]" data-testid="boost-body-title">{
          boost.title || boost.link_title
        }</h1>
      }
      { Boolean(boost.message) && 
        <p className={"text-md text-primary break-words"} data-testid="boost-body-message">
          <MindsLinkify>{boost.message || boost.link_title}</MindsLinkify>
        </p>
      }
    </div>
  );
}