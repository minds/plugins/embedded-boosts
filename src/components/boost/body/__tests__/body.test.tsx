import '@testing-library/jest-dom'
import { render, screen, within } from '@testing-library/react'
import { BoostBody } from '../body'
import { mockBoostEntity } from '../../../../../__mocks__/models/mock-boost-entity'
import { BoostEntity } from '../../../../types/boost'

describe('BoostBody', () => {
  it('renders a heading', () => {
    const { container } = render(<BoostBody boost={mockBoostEntity}/>);
    expect(container).toMatchSnapshot();

    const { getByText } = within(screen.getByTestId('boost-body-title'))
    const headingText: HTMLElement = getByText(mockBoostEntity.title);
    expect(headingText).toBeInTheDocument()
    expect(headingText.nodeName).toBe('H1')
  });

  it('renders a body', () => {
    const { container } = render(<BoostBody boost={mockBoostEntity}/>);
    expect(container).toMatchSnapshot();

    const { getByText } = within(screen.getByTestId('boost-body-message'))
    const messageText: HTMLElement = getByText(mockBoostEntity.message);
    expect(messageText).toBeInTheDocument()
    expect(messageText.nodeName).toBe('P')
  });

  it('renders a body with correct classes for a boost body', () => {
    const { container } = render(<BoostBody boost={mockBoostEntity}/>);
    expect(container).toMatchSnapshot();

    const bodyContainerElement: HTMLElement = screen.getByTestId('boost-body-container');
    expect(bodyContainerElement).toBeInTheDocument();
    expect(bodyContainerElement.nodeName).toBe('DIV');
    expect(bodyContainerElement).toHaveClass('flex-1 overflow-auto mt-[12px] px-[16px] mb-0 h-md:mb-[8px]');
  });

  it('renders a body with correct classes for no boost body', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      title: "",
      message: "",
      link_title: ""
    }

    const { container } = render(<BoostBody boost={_mockBoostEntity}/>);
    expect(container).toMatchSnapshot();

    const bodyContainerElement: HTMLElement = screen.getByTestId('boost-body-container');
    expect(bodyContainerElement).toBeInTheDocument();
    expect(bodyContainerElement.nodeName).toBe('DIV');
    expect(bodyContainerElement).not.toHaveClass('flex-1 overflow-auto mt-[12px] px-[16px] mb-0 h-md:mb-[8px]');
  });
})