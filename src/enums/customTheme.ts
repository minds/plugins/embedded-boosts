/** Custom theme enum. */
export enum CustomTheme {
  FishTank = 'fishtank'
}

/** Returns whether a theme is custom by string key. */
export const isCustomTheme = (theme: string): boolean => {
  return Object.values(CustomTheme).includes(theme as CustomTheme);
}