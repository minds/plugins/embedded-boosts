/** Boost goal button text. */
export enum BoostGoalButtonText {
  SUBSCRIBE_TO_MY_CHANNEL = 1,
  GET_CONNECTED = 2,
  STAY_IN_THE_LOOP = 3,
  LEARN_MORE = 4,
  GET_STARTED = 5,
  SIGN_UP = 6,
  TRY_FOR_FREE = 7,
  SHOP_NOW = 8,
  BUY_NOW = 9
}