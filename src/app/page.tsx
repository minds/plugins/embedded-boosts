"use client";

import { ApolloProvider } from '@apollo/client';
import { buildApolloClient } from '../utils/apollo-client/apollo-client';
import { Main } from '../components/boost/main/main';
import { ReadonlyURLSearchParams, useSearchParams } from 'next/navigation';
import { ReactNode, Suspense } from 'react';

/**
 * Home page component.
 * @param { PageProps } props - Page props.
 * @returns { ReactNode } - Page component.
 */
export default function Home(): ReactNode {   
  const searchParams: ReadonlyURLSearchParams = useSearchParams();
  let siteUrl: string|null = searchParams.get('siteUrl') ?? null;

  if (!siteUrl) {
    if (typeof window !== 'undefined') {
      siteUrl = document?.location?.origin;
    } else {
      return <p>No siteUrl searchParam found</p>;
    }
  }

  return (
    <ApolloProvider client={buildApolloClient(siteUrl)}>
      <Main siteUrl={siteUrl}/>
    </ApolloProvider>
  );
}