import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { JetBrains_Mono } from "next/font/google";
import "./globals.css";
import LayoutBody from "../components/common/layout-body/layout-body";

/** Metadata for the page. */
export const metadata: Metadata = {
  title: "Minds Embedded Boost",
  description: "Embeddable ad unit for Minds Boost"
};

/** Root layout component. */
export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <LayoutBody>{children}</LayoutBody>
    </html>
  );
}