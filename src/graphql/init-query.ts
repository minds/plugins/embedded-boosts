import { gql } from "@apollo/client";

/** Query with init data for the app. */
export const INIT_QUERY = gql`query GetBoostsWithConfigs {
    boosts(first: 1) {
        edges {
            node {
                legacy
            }
        }
    }
    primary_color: config(key: "theme_override.primary_color")
    color_scheme: config(key: "theme_override.color_scheme")
    posthog_host: config(key: "posthog.host")
    posthog_api_key: config(key: "posthog.api_key")
    tenant_id: config(key: "tenant_id")
}`;