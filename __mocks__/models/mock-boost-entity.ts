import { BoostGoalButtonText } from "../../src/enums/goalButtonText";

export const mockBoostEntity = {
	guid: '1234567890123456',
	message: 'This is a message',
	title: 'This is a title',
	custom_type: null,
	custom_data: null,
	thumbnail_src: 'https://www.minds.com/static/en/assets/logos/bulb.svg',
	perma_url: 'https://www.minds.com/',
	thumbnails: {
		"xlarge": 'https://www.minds.com/static/en/assets/logos/bulb.svg'
	},
	link_title: null,
	boosted_guid: '2234567890123456',
	goal_button_text: BoostGoalButtonText.SIGN_UP,
	goal_button_url: 'https://www.minds.com/canary'
}